﻿namespace Phong_tro
{
    partial class addClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tborigin = new System.Windows.Forms.TextBox();
            this.Origin = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tbsex = new System.Windows.Forms.TextBox();
            this.sex = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tbage = new System.Windows.Forms.TextBox();
            this.age = new System.Windows.Forms.Label();
            this.bLogon = new System.Windows.Forms.Button();
            this.bExit = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbName = new System.Windows.Forms.TextBox();
            this.name = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tbcmnd = new System.Windows.Forms.TextBox();
            this.cmnd = new System.Windows.Forms.Label();
            this.lbWorkingRoom = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tbSDT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.bLogon);
            this.panel1.Controls.Add(this.bExit);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(12, 32);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(381, 295);
            this.panel1.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.tborigin);
            this.panel6.Controls.Add(this.Origin);
            this.panel6.Location = new System.Drawing.Point(3, 148);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(375, 42);
            this.panel6.TabIndex = 4;
            // 
            // tborigin
            // 
            this.tborigin.Location = new System.Drawing.Point(143, 9);
            this.tborigin.Name = "tborigin";
            this.tborigin.Size = new System.Drawing.Size(229, 20);
            this.tborigin.TabIndex = 1;
            // 
            // Origin
            // 
            this.Origin.AutoSize = true;
            this.Origin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Origin.Location = new System.Drawing.Point(3, 10);
            this.Origin.Name = "Origin";
            this.Origin.Size = new System.Drawing.Size(97, 20);
            this.Origin.TabIndex = 0;
            this.Origin.Text = "Quê quán: ";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.tbsex);
            this.panel5.Controls.Add(this.sex);
            this.panel5.Location = new System.Drawing.Point(216, 100);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(162, 42);
            this.panel5.TabIndex = 3;
            // 
            // tbsex
            // 
            this.tbsex.Location = new System.Drawing.Point(65, 10);
            this.tbsex.Name = "tbsex";
            this.tbsex.Size = new System.Drawing.Size(84, 20);
            this.tbsex.TabIndex = 0;
            // 
            // sex
            // 
            this.sex.AutoSize = true;
            this.sex.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sex.Location = new System.Drawing.Point(3, 10);
            this.sex.Name = "sex";
            this.sex.Size = new System.Drawing.Size(51, 20);
            this.sex.TabIndex = 0;
            this.sex.Text = "Giới: ";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tbage);
            this.panel4.Controls.Add(this.age);
            this.panel4.Location = new System.Drawing.Point(3, 100);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(138, 42);
            this.panel4.TabIndex = 2;
            // 
            // tbage
            // 
            this.tbage.Location = new System.Drawing.Point(65, 10);
            this.tbage.Name = "tbage";
            this.tbage.Size = new System.Drawing.Size(64, 20);
            this.tbage.TabIndex = 0;
            // 
            // age
            // 
            this.age.AutoSize = true;
            this.age.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.age.Location = new System.Drawing.Point(3, 10);
            this.age.Name = "age";
            this.age.Size = new System.Drawing.Size(53, 20);
            this.age.TabIndex = 0;
            this.age.Text = "Tuổi: ";
            // 
            // bLogon
            // 
            this.bLogon.Location = new System.Drawing.Point(222, 243);
            this.bLogon.Name = "bLogon";
            this.bLogon.Size = new System.Drawing.Size(75, 49);
            this.bLogon.TabIndex = 5;
            this.bLogon.Text = "Đăng ký";
            this.bLogon.UseVisualStyleBackColor = true;
            this.bLogon.Click += new System.EventHandler(this.bLogon_Click);
            // 
            // bExit
            // 
            this.bExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bExit.Location = new System.Drawing.Point(306, 243);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(75, 49);
            this.bExit.TabIndex = 7;
            this.bExit.Text = "Xong";
            this.bExit.UseVisualStyleBackColor = true;
            this.bExit.Click += new System.EventHandler(this.bExit_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tbName);
            this.panel2.Controls.Add(this.name);
            this.panel2.Location = new System.Drawing.Point(3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(375, 42);
            this.panel2.TabIndex = 0;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(143, 9);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(229, 20);
            this.tbName.TabIndex = 1;
            this.tbName.TextChanged += new System.EventHandler(this.tbName_TextChanged);
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.Location = new System.Drawing.Point(3, 10);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(44, 20);
            this.name.TabIndex = 0;
            this.name.Text = "Tên:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tbcmnd);
            this.panel3.Controls.Add(this.cmnd);
            this.panel3.Location = new System.Drawing.Point(3, 52);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(375, 42);
            this.panel3.TabIndex = 1;
            // 
            // tbcmnd
            // 
            this.tbcmnd.Location = new System.Drawing.Point(143, 9);
            this.tbcmnd.Name = "tbcmnd";
            this.tbcmnd.Size = new System.Drawing.Size(229, 20);
            this.tbcmnd.TabIndex = 1;
            // 
            // cmnd
            // 
            this.cmnd.AutoSize = true;
            this.cmnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmnd.Location = new System.Drawing.Point(3, 10);
            this.cmnd.Name = "cmnd";
            this.cmnd.Size = new System.Drawing.Size(97, 20);
            this.cmnd.TabIndex = 0;
            this.cmnd.Text = "Số CMND: ";
            // 
            // lbWorkingRoom
            // 
            this.lbWorkingRoom.AutoSize = true;
            this.lbWorkingRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWorkingRoom.Location = new System.Drawing.Point(16, 9);
            this.lbWorkingRoom.Name = "lbWorkingRoom";
            this.lbWorkingRoom.Size = new System.Drawing.Size(97, 20);
            this.lbWorkingRoom.TabIndex = 9;
            this.lbWorkingRoom.Text = "Quê quán: ";
            this.lbWorkingRoom.VisibleChanged += new System.EventHandler(this.tbWorkingRoom_VisibleChanged);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.tbSDT);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Location = new System.Drawing.Point(3, 196);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(375, 42);
            this.panel7.TabIndex = 8;
            // 
            // tbSDT
            // 
            this.tbSDT.Location = new System.Drawing.Point(143, 9);
            this.tbSDT.Name = "tbSDT";
            this.tbSDT.Size = new System.Drawing.Size(229, 20);
            this.tbSDT.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "SDT:";
            // 
            // addClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 339);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbWorkingRoom);
            this.Name = "addClient";
            this.Text = "addClient";
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbWorkingRoom;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox tborigin;
        private System.Windows.Forms.Label Origin;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox tbsex;
        private System.Windows.Forms.Label sex;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox tbage;
        private System.Windows.Forms.Label age;
        private System.Windows.Forms.Button bLogon;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox tbcmnd;
        private System.Windows.Forms.Label cmnd;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox tbSDT;
        private System.Windows.Forms.Label label1;
    }
}