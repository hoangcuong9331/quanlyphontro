﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Phong_tro
{
    public partial class Setup : Form
    {
        public Setup()
        {
            InitializeComponent();
        }

        private void bSetup_Click(object sender, EventArgs e)
        {
            string query =  "exec dbo.Setup @RoomNumber = " + Int32.Parse(tbRoomNumber.Text) + ", @RoomPrice = " + Int32.Parse(tbPrice.Text) +
                ", @ElecPrice = " + Int32.Parse(tbElecPrice.Text) + ", @WaterPrice = " + Int32.Parse(tbWaterPrice.Text);
            try
            {
                DAO.DataProvider.Instance.ExecuteNonQuery(query);
                MessageBox.Show("Setup thành công");
                Close();
            }
            catch
            {
                MessageBox.Show("Lỗi");
            }
        }

        private void bExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
