﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phong_tro.DTO
{
    public class Room
    {
        public Room(int maphong, int trong, string mieuta, string tenphong)
        {
            this.MaPhong = maphong;
            this.TRONG = trong;
            this.MieuTa = mieuta;
            this.TenPhong = tenphong;
        }

        public Room()
        {
            this.MaPhong = -1;
            this.TRONG = 1;
            this.MieuTa = "";
            this.TenPhong = "";
        }

        public Room(DataRow row)
        {
            this.MaPhong = (int)row["MaPhong"];
            this.Gia = (int)row["Gia"];
            this.TenPhong = row["TenPhong"].ToString();
            this.TRONG = (int)row["TRONG"];
            this.MieuTa = row["MieuTa"].ToString();
        }

        private int maPhong;

        public int MaPhong
        {
            get { return maPhong; }
            set { maPhong = value; }
        }

        private int trong;

        public int TRONG
        {
            get { return trong; }
            set { trong = value; }
        }

        private int gia;

        public int Gia
        {
            get { return gia; }
            set { gia = value; }
        }

        private string mieuTa;

        public string MieuTa
        {
            get { return mieuTa; }
            set { mieuTa = value; }
        }

        private string tenPhong;

        public string TenPhong
        {
            get { return tenPhong; }
            set { tenPhong = value; }
        }
    }
}
