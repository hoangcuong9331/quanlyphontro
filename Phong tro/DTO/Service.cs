﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace Phong_tro.DTO
{
    public class Service
    {
        public Service(DataRow row)
        {
            this.MaPhong = (int)row["MaPhong"];
            this.Gia = (int)row["Gia"];
            this.Ten = row["Ten"].ToString();
        }

        private string ten;

        public string Ten
        {
            get { return ten; }
            set { ten = value; }
        }
        private int maPhong;

        public int MaPhong
        {
            get { return maPhong; }
            set { maPhong = value; }
        }
        private int gia;

        public int Gia
        {
            get { return gia; }
            set { gia = value; }
        }
    }
}
