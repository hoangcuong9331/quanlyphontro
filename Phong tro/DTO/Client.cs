﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Phong_tro.DTO
{
    public class Client
    {
        public Client(DataRow row)
        {
            CMND = (int)row["CMND"];
            MaDK = (int)row["MaDK"];
            Ten = (string)row["Ten"];
            Tuoi = (int)row["Tuoi"];
            Gioi = (string)row["Gioi"];
            QueQuan = (string)row["QueQuan"];
            SDT = (int)row["SDT"];
        }

        private int cMND;
        public int CMND
        {
            get { return cMND; }
            set { cMND = value; }
        }
        private int maDK;
        public int MaDK
        {
            get { return maDK; }
            set { maDK = value; }
        }
        private string ten;
        public string Ten
        {
            get { return ten; }
            set { ten = value; }
        }
        private int tuoi;
        public int Tuoi
        {
            get { return tuoi; }
            set { tuoi = value; }
        }
        private int sDT;
        public int SDT
        {
            get { return sDT; }
            set { sDT = value; }
        }
        private string gioi;
        public string Gioi
        {
            get { return gioi; }
            set { gioi = value; }
        }
        private string queQuan;
        public string QueQuan
        {
            get { return queQuan; }
            set { queQuan = value; }
        }
    }
}
