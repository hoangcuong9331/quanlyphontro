﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Phong_tro.DTO
{
    public class Bill
    {
        public Bill(DataRow row)
        {
            SoDien = (int)row["SoDien"];
            SoNuoc = (int)row["SoNuoc"];
            MaPhong = (int)row["MaPhong"];
            Ky = (int)row["Ky"];
            NgayTinh = row["NgayTinh"].ToString();
        }

        private int soDien;
        public int SoDien
        {
            get { return soDien; }
            set { soDien = value; }
        }
        private int soNuoc;
        public int SoNuoc
        {
            get { return soNuoc; }
            set { soNuoc = value; }
        }
        private int maPhong;
        public int MaPhong
        {
            get { return maPhong; }
            set { maPhong = value; }
        }
        private int ky;
        public int Ky
        {
            get { return ky; }
            set { ky = value; }
        }
        private string ngayTinh;
        public string NgayTinh
        {
            get { return ngayTinh; }
            set { ngayTinh = value; }
        }
    }
}
