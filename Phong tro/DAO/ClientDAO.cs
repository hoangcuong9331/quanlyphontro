﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Phong_tro.DAO
{
    public class ClientDAO
    {
        private static ClientDAO instance;

        public static ClientDAO Instance
        {
            get { if (instance == null) instance = new ClientDAO(); return ClientDAO.instance; }
            private set { ClientDAO.instance = value; }
        }

        private ClientDAO() { }

        public List<DTO.Client> LoadClientListByRoomID(string rID)
        {
            List<DTO.Client> clientList = new List<DTO.Client>();

            string query = "exec dbo.USP_LoadClientListByRoomID @rID="+ rID;
            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                DTO.Client client = new DTO.Client(item);

                clientList.Add(client);
            }

            return clientList;
        }
    }
}
