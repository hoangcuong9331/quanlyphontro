﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Phong_tro.DAO
{
    public class BillDAO
    {

        private static BillDAO instance;

        public static BillDAO Instance
        {
            get { if (instance == null) instance = new BillDAO(); return BillDAO.instance; }
            private set { BillDAO.instance = value; }
        }

        private BillDAO() { }

        public List<DTO.Bill> LoadBillListByRoom(int IDroom)
        {
            List<DTO.Bill> billList = new List<DTO.Bill>();

            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetBillListByRoom @IDRoom = " + IDroom);

            foreach (DataRow item in data.Rows)
            {
                DTO.Bill bill = new DTO.Bill(item);

                billList.Add(bill);
            }

            return billList;
        }
    }
}
