﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace Phong_tro.DAO
{
    public class ServiceDAO
    {
        private static ServiceDAO instance;

        public static ServiceDAO Instance
        {
            get { if (instance == null) instance = new ServiceDAO(); return ServiceDAO.instance; }
            private set { ServiceDAO.instance = value; }
        }

        private ServiceDAO() { }

        public List<DTO.Service> LoadServiceListByRoomID(string rID)
        {
            List<DTO.Service> serviceList = new List<DTO.Service>();

            string query = "exec dbo.USP_LoadServiceListByRoomID @rID=" + rID;
            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                DTO.Service service= new DTO.Service(item);

                serviceList.Add(service);
            }

            return serviceList;
        }
        public List<DTO.Service> LoadServiceList()
        {
            List<DTO.Service> serviceList = new List<DTO.Service>();

            string query = "exec dbo.USP_LoadServiceList";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                DTO.Service service = new DTO.Service(item);

                serviceList.Add(service);
            }

            return serviceList;
        }
    }
}
