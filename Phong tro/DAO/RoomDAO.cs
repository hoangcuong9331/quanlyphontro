﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace Phong_tro.DAO
{
    public class RoomDAO
    {
        private static RoomDAO instance;

        public static RoomDAO Instance
        {
            get { if (instance == null) instance = new RoomDAO(); return RoomDAO.instance; }
            private set { RoomDAO.instance = value; }
        }

        private RoomDAO() { }

        public List<DTO.Room> LoadRoomList()
        {
            List<DTO.Room> roomList = new List<DTO.Room>();

            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetRoomList");

            foreach(DataRow item in data.Rows)
            {
                DTO.Room room = new DTO.Room(item);

                roomList.Add(room);
            }

            return roomList;
        }
        public DTO.Room LoadRoomByID(string id)
        {
            DTO.Room room = new DTO.Room();
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetRoomById @IDRoom = " + id);

            foreach (DataRow item in data.Rows)
            {
                room = new DTO.Room(item);
            }

            return room;
        }
    }
}
