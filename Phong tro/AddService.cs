﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Phong_tro
{
    public partial class AddService : Form
    {
        private string workingRoom;
        public string WorkingRoom
        {
            get { return workingRoom; }
            set { workingRoom = value; }
        }

        public AddService()
        {
            InitializeComponent();
            List<DTO.Room> listRoom = DAO.RoomDAO.Instance.LoadRoomList();
            DTO.Room allRoom = new DTO.Room();
            allRoom.TenPhong = "tất cả";
            listRoom.Add(allRoom);
            cbRoom.DataSource = listRoom;
            cbRoom.DisplayMember = "TenPhong";
        }

        private void bExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bElecChange_Click(object sender, EventArgs e)
        {
            string query = "exec dbo.DoiGiaDien @gia = " + tbElec.Text;
            try
            {
                DAO.DataProvider.Instance.ExecuteNonQuery(query);
                refreshdgvSer();
            }
            catch
            {
                MessageBox.Show("Lỗi rồi!");
            }
        }

        private void bWaterChange_Click(object sender, EventArgs e)
        {
            string query = "exec dbo.DoiGiaNuoc @gia = " + tbWater.Text;
            try
            {
                DAO.DataProvider.Instance.ExecuteNonQuery(query);
                refreshdgvSer();
            }
            catch
            {
                MessageBox.Show("Lỗi rồi!");
            }
        }

        private void bAddSer_Click(object sender, EventArgs e)
        {
            if (cbRoom.SelectedIndex == cbRoom.Items.Count - 1)
            {
                string query = "exec dbo.AddSerForAll @Ten = N'" + tbSerName.Text + "', @Gia = " + tbSerPrice.Text;
                try
                {
                    DAO.DataProvider.Instance.ExecuteNonQuery(query);
                    refreshdgvSer();
                }
                catch
                {
                    MessageBox.Show("Lỗi rồi!");
                }
            }
            else
            {
                string query = "exec dbo.AddSerByRoomID @ten = N'" + tbSerName.Text + "', @gia = " + tbSerPrice.Text
                    + ", @rID = " + workingRoom;
                try
                {
                    DAO.DataProvider.Instance.ExecuteNonQuery(query);
                    refreshdgvSer();
                }
                catch
                {
                    MessageBox.Show("Lỗi rồi!");
                }
            }
        }

        private void cbRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            
            List<DTO.Room> listRoom = DAO.RoomDAO.Instance.LoadRoomList();
            List<DTO.Service> listSer;
            if (cb.SelectedIndex != listRoom.Count)
            {
                workingRoom = listRoom[cb.SelectedIndex].MaPhong.ToString();
                listSer = DAO.ServiceDAO.Instance.LoadServiceListByRoomID(workingRoom);
                dgvSer.DataSource = listSer;
            }
            else
            {
                workingRoom = (-1).ToString();
                listSer = DAO.ServiceDAO.Instance.LoadServiceList();
                dgvSer.DataSource = listSer;
            }
        }

        private void refreshdgvSer()
        {
            if(workingRoom != (-1).ToString())
            {
                List<DTO.Service> listSer = DAO.ServiceDAO.Instance.LoadServiceListByRoomID(workingRoom);
                dgvSer.DataSource = listSer;
            }
            else
            {
                List<DTO.Service> listSer = DAO.ServiceDAO.Instance.LoadServiceList();
                dgvSer.DataSource = listSer;
            }
        }

        private void AddService_Load(object sender, EventArgs e)
        {

        }
    }
}
