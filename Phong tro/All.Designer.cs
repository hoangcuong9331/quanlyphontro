﻿namespace Phong_tro
{
    partial class All
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Logout = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bAnalyzed = new System.Windows.Forms.Button();
            this.bAddService = new System.Windows.Forms.Button();
            this.bAddroom = new System.Windows.Forms.Button();
            this.bReset = new System.Windows.Forms.Button();
            this.dgvRoom = new System.Windows.Forms.DataGridView();
            this.bElement = new System.Windows.Forms.Button();
            this.bClient = new System.Windows.Forms.Button();
            this.bBill = new System.Windows.Forms.Button();
            this.bAddClien = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TableName = new System.Windows.Forms.Label();
            this.bEnempty = new System.Windows.Forms.Button();
            this.bChangeRoom = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoom)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Danh sách phòng";
            // 
            // Logout
            // 
            this.Logout.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Logout.Location = new System.Drawing.Point(691, 274);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(91, 34);
            this.Logout.TabIndex = 3;
            this.Logout.Text = "Đăng xuất";
            this.Logout.UseVisualStyleBackColor = true;
            this.Logout.Click += new System.EventHandler(this.Logout_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bAnalyzed);
            this.panel2.Controls.Add(this.bAddService);
            this.panel2.Controls.Add(this.bAddroom);
            this.panel2.Controls.Add(this.bChangeRoom);
            this.panel2.Controls.Add(this.bReset);
            this.panel2.Controls.Add(this.dgvRoom);
            this.panel2.Controls.Add(this.bElement);
            this.panel2.Controls.Add(this.bClient);
            this.panel2.Location = new System.Drawing.Point(396, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(390, 200);
            this.panel2.TabIndex = 5;
            // 
            // bAnalyzed
            // 
            this.bAnalyzed.Location = new System.Drawing.Point(198, 3);
            this.bAnalyzed.Name = "bAnalyzed";
            this.bAnalyzed.Size = new System.Drawing.Size(91, 34);
            this.bAnalyzed.TabIndex = 7;
            this.bAnalyzed.Text = "Phân tích";
            this.bAnalyzed.UseVisualStyleBackColor = true;
            this.bAnalyzed.Click += new System.EventHandler(this.bAnalyzed_Click);
            // 
            // bAddService
            // 
            this.bAddService.Location = new System.Drawing.Point(101, 3);
            this.bAddService.Name = "bAddService";
            this.bAddService.Size = new System.Drawing.Size(91, 34);
            this.bAddService.TabIndex = 4;
            this.bAddService.Text = "Dịch vụ";
            this.bAddService.UseVisualStyleBackColor = true;
            this.bAddService.Click += new System.EventHandler(this.bAddService_Click);
            // 
            // bAddroom
            // 
            this.bAddroom.Location = new System.Drawing.Point(7, 3);
            this.bAddroom.Name = "bAddroom";
            this.bAddroom.Size = new System.Drawing.Size(91, 34);
            this.bAddroom.TabIndex = 3;
            this.bAddroom.Text = "Thêm phòng";
            this.bAddroom.UseVisualStyleBackColor = true;
            this.bAddroom.Click += new System.EventHandler(this.bAddroom_Click);
            // 
            // bReset
            // 
            this.bReset.Location = new System.Drawing.Point(295, 3);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(91, 34);
            this.bReset.TabIndex = 5;
            this.bReset.Text = "Reset";
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            // 
            // dgvRoom
            // 
            this.dgvRoom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRoom.Location = new System.Drawing.Point(7, 43);
            this.dgvRoom.Name = "dgvRoom";
            this.dgvRoom.Size = new System.Drawing.Size(383, 107);
            this.dgvRoom.TabIndex = 6;
            this.dgvRoom.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRoom_CellContentClick);
            // 
            // bElement
            // 
            this.bElement.Enabled = false;
            this.bElement.Location = new System.Drawing.Point(4, 156);
            this.bElement.Name = "bElement";
            this.bElement.Size = new System.Drawing.Size(91, 34);
            this.bElement.TabIndex = 0;
            this.bElement.Text = "Thông tin phòng";
            this.bElement.UseVisualStyleBackColor = true;
            this.bElement.Click += new System.EventHandler(this.bElement_Click);
            // 
            // bClient
            // 
            this.bClient.Location = new System.Drawing.Point(101, 156);
            this.bClient.Name = "bClient";
            this.bClient.Size = new System.Drawing.Size(91, 34);
            this.bClient.TabIndex = 1;
            this.bClient.Text = "DS khách hàng";
            this.bClient.UseVisualStyleBackColor = true;
            this.bClient.Click += new System.EventHandler(this.bClient_Click);
            // 
            // bBill
            // 
            this.bBill.Enabled = false;
            this.bBill.Location = new System.Drawing.Point(594, 236);
            this.bBill.Name = "bBill";
            this.bBill.Size = new System.Drawing.Size(91, 74);
            this.bBill.TabIndex = 5;
            this.bBill.Text = "Tính tiền";
            this.bBill.UseVisualStyleBackColor = true;
            this.bBill.Click += new System.EventHandler(this.bBill_Click);
            // 
            // bAddClien
            // 
            this.bAddClien.Enabled = false;
            this.bAddClien.Location = new System.Drawing.Point(400, 238);
            this.bAddClien.Name = "bAddClien";
            this.bAddClien.Size = new System.Drawing.Size(91, 70);
            this.bAddClien.TabIndex = 1;
            this.bAddClien.Text = "Thêm khách hàng";
            this.bAddClien.UseVisualStyleBackColor = true;
            this.bAddClien.Click += new System.EventHandler(this.bAddClien_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(13, 32);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(377, 276);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // TableName
            // 
            this.TableName.AutoSize = true;
            this.TableName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TableName.Location = new System.Drawing.Point(392, 9);
            this.TableName.Name = "TableName";
            this.TableName.Size = new System.Drawing.Size(90, 20);
            this.TableName.TabIndex = 4;
            this.TableName.Text = "Bảng biểu";
            // 
            // bEnempty
            // 
            this.bEnempty.Enabled = false;
            this.bEnempty.Location = new System.Drawing.Point(497, 238);
            this.bEnempty.Name = "bEnempty";
            this.bEnempty.Size = new System.Drawing.Size(91, 70);
            this.bEnempty.TabIndex = 2;
            this.bEnempty.Text = "Trả phòng";
            this.bEnempty.UseVisualStyleBackColor = true;
            this.bEnempty.Click += new System.EventHandler(this.Enempty_Click);
            // 
            // bChangeRoom
            // 
            this.bChangeRoom.Enabled = false;
            this.bChangeRoom.Location = new System.Drawing.Point(295, 156);
            this.bChangeRoom.Name = "bChangeRoom";
            this.bChangeRoom.Size = new System.Drawing.Size(91, 34);
            this.bChangeRoom.TabIndex = 2;
            this.bChangeRoom.Text = "Chuyển phòng";
            this.bChangeRoom.UseVisualStyleBackColor = true;
            this.bChangeRoom.Click += new System.EventHandler(this.bChangeRoom_Click);
            // 
            // All
            // 
            this.AcceptButton = this.bAddClien;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Logout;
            this.ClientSize = new System.Drawing.Size(798, 320);
            this.Controls.Add(this.bEnempty);
            this.Controls.Add(this.bBill);
            this.Controls.Add(this.bAddClien);
            this.Controls.Add(this.TableName);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.Logout);
            this.Controls.Add(this.label1);
            this.Name = "All";
            this.Text = "Tổng quan";
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoom)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Logout;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgvRoom;
        private System.Windows.Forms.Button bElement;
        private System.Windows.Forms.Button bClient;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label TableName;
        private System.Windows.Forms.Button bAddClien;
        private System.Windows.Forms.Button bAddroom;
        private System.Windows.Forms.Button bEnempty;
        private System.Windows.Forms.Button bBill;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.Button bAddService;
        private System.Windows.Forms.Button bAnalyzed;
        private System.Windows.Forms.Button bChangeRoom;
    }
}