﻿namespace Phong_tro
{
    partial class AddService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvSer = new System.Windows.Forms.DataGridView();
            this.cbRoom = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.bAddSer = new System.Windows.Forms.Button();
            this.tbSerPrice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSerName = new System.Windows.Forms.TextBox();
            this.age = new System.Windows.Forms.Label();
            this.bExit = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bElecChange = new System.Windows.Forms.Button();
            this.tbElec = new System.Windows.Forms.TextBox();
            this.Elec = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.bWaterChange = new System.Windows.Forms.Button();
            this.tbWater = new System.Windows.Forms.TextBox();
            this.cmnd = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSer)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvSer);
            this.panel1.Controls.Add(this.cbRoom);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(307, 227);
            this.panel1.TabIndex = 2;
            // 
            // dgvSer
            // 
            this.dgvSer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSer.Location = new System.Drawing.Point(3, 30);
            this.dgvSer.Name = "dgvSer";
            this.dgvSer.Size = new System.Drawing.Size(304, 121);
            this.dgvSer.TabIndex = 0;
            // 
            // cbRoom
            // 
            this.cbRoom.FormattingEnabled = true;
            this.cbRoom.Location = new System.Drawing.Point(3, 3);
            this.cbRoom.Name = "cbRoom";
            this.cbRoom.Size = new System.Drawing.Size(104, 21);
            this.cbRoom.TabIndex = 3;
            this.cbRoom.SelectedIndexChanged += new System.EventHandler(this.cbRoom_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.bAddSer);
            this.panel4.Controls.Add(this.tbSerPrice);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.tbSerName);
            this.panel4.Controls.Add(this.age);
            this.panel4.Location = new System.Drawing.Point(3, 153);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(304, 71);
            this.panel4.TabIndex = 2;
            // 
            // bAddSer
            // 
            this.bAddSer.Location = new System.Drawing.Point(203, 23);
            this.bAddSer.Name = "bAddSer";
            this.bAddSer.Size = new System.Drawing.Size(91, 33);
            this.bAddSer.TabIndex = 4;
            this.bAddSer.Text = "Thêm";
            this.bAddSer.UseVisualStyleBackColor = true;
            this.bAddSer.Click += new System.EventHandler(this.bAddSer_Click);
            // 
            // tbSerPrice
            // 
            this.tbSerPrice.Location = new System.Drawing.Point(123, 36);
            this.tbSerPrice.Name = "tbSerPrice";
            this.tbSerPrice.Size = new System.Drawing.Size(64, 20);
            this.tbSerPrice.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Giá:";
            // 
            // tbSerName
            // 
            this.tbSerName.Location = new System.Drawing.Point(123, 10);
            this.tbSerName.Name = "tbSerName";
            this.tbSerName.Size = new System.Drawing.Size(64, 20);
            this.tbSerName.TabIndex = 1;
            // 
            // age
            // 
            this.age.AutoSize = true;
            this.age.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.age.Location = new System.Drawing.Point(3, 10);
            this.age.Name = "age";
            this.age.Size = new System.Drawing.Size(119, 20);
            this.age.TabIndex = 0;
            this.age.Text = "Thêm dịch vụ:";
            // 
            // bExit
            // 
            this.bExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bExit.Location = new System.Drawing.Point(562, 190);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(75, 49);
            this.bExit.TabIndex = 3;
            this.bExit.Text = "Xong";
            this.bExit.UseVisualStyleBackColor = true;
            this.bExit.Click += new System.EventHandler(this.bExit_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView2);
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Controls.Add(this.bElecChange);
            this.panel2.Controls.Add(this.tbElec);
            this.panel2.Controls.Add(this.Elec);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(304, 42);
            this.panel2.TabIndex = 0;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(107, -31);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(202, 21);
            this.dataGridView2.TabIndex = 4;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(36, 99);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 47);
            this.dataGridView1.TabIndex = 4;
            // 
            // bElecChange
            // 
            this.bElecChange.Location = new System.Drawing.Point(203, 4);
            this.bElecChange.Name = "bElecChange";
            this.bElecChange.Size = new System.Drawing.Size(91, 34);
            this.bElecChange.TabIndex = 2;
            this.bElecChange.Text = "Thay đổi";
            this.bElecChange.UseVisualStyleBackColor = true;
            this.bElecChange.Click += new System.EventHandler(this.bElecChange_Click);
            // 
            // tbElec
            // 
            this.tbElec.Location = new System.Drawing.Point(90, 10);
            this.tbElec.Name = "tbElec";
            this.tbElec.Size = new System.Drawing.Size(97, 20);
            this.tbElec.TabIndex = 1;
            // 
            // Elec
            // 
            this.Elec.AutoSize = true;
            this.Elec.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Elec.Location = new System.Drawing.Point(3, 10);
            this.Elec.Name = "Elec";
            this.Elec.Size = new System.Drawing.Size(81, 20);
            this.Elec.TabIndex = 0;
            this.Elec.Text = "Giá điện:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView3);
            this.panel3.Controls.Add(this.bWaterChange);
            this.panel3.Controls.Add(this.tbWater);
            this.panel3.Controls.Add(this.cmnd);
            this.panel3.Location = new System.Drawing.Point(3, 47);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(304, 42);
            this.panel3.TabIndex = 1;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(153, -99);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(152, 38);
            this.dataGridView3.TabIndex = 0;
            // 
            // bWaterChange
            // 
            this.bWaterChange.Location = new System.Drawing.Point(203, 4);
            this.bWaterChange.Name = "bWaterChange";
            this.bWaterChange.Size = new System.Drawing.Size(91, 34);
            this.bWaterChange.TabIndex = 2;
            this.bWaterChange.Text = "Thay đổi";
            this.bWaterChange.UseVisualStyleBackColor = true;
            this.bWaterChange.Click += new System.EventHandler(this.bWaterChange_Click);
            // 
            // tbWater
            // 
            this.tbWater.Location = new System.Drawing.Point(90, 10);
            this.tbWater.Name = "tbWater";
            this.tbWater.Size = new System.Drawing.Size(97, 20);
            this.tbWater.TabIndex = 1;
            // 
            // cmnd
            // 
            this.cmnd.AutoSize = true;
            this.cmnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmnd.Location = new System.Drawing.Point(3, 10);
            this.cmnd.Name = "cmnd";
            this.cmnd.Size = new System.Drawing.Size(86, 20);
            this.cmnd.TabIndex = 0;
            this.cmnd.Text = "Giá nước:";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel2);
            this.panel5.Controls.Add(this.panel3);
            this.panel5.Location = new System.Drawing.Point(330, 15);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(307, 93);
            this.panel5.TabIndex = 4;
            // 
            // AddService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 245);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bExit);
            this.Name = "AddService";
            this.Text = "Dịch vụ";
            this.Load += new System.EventHandler(this.AddService_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSer)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox tbSerName;
        private System.Windows.Forms.Label age;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tbElec;
        private System.Windows.Forms.Label Elec;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox tbWater;
        private System.Windows.Forms.Label cmnd;
        private System.Windows.Forms.Button bAddSer;
        private System.Windows.Forms.TextBox tbSerPrice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bElecChange;
        private System.Windows.Forms.Button bWaterChange;
        private System.Windows.Forms.ComboBox cbRoom;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView dgvSer;
    }
}