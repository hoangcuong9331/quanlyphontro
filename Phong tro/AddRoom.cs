﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace Phong_tro
{
    public partial class AddRoom : Form
    {
        public AddRoom()
        {
            InitializeComponent();
        }

        private void BAdd_Click(object sender, EventArgs e)
        {
            if(tbPrice.Text !="" && tbRoomName.Text!="")
            {
                try
                {
                    string query = "exec dbo.createRoom @tbPrice = " + tbPrice.Text + ", @tbRoomName = N'" + tbRoomName.Text + "', @tbDescribe = N'" + tbDescribe.Text + "'";
                    DAO.DataProvider.Instance.ExecuteNonQuery(query);
                    MessageBox.Show("Thêm thành công!", "Thông báo", MessageBoxButtons.OK);
                }
                catch
                {
                    MessageBox.Show("Gặp lỗi!");
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa điền đủ thông tin!");
            }

        }

        private void bExit_Click(object sender, EventArgs e)
        { 
            Close();
        }

        private void tbRoomName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
