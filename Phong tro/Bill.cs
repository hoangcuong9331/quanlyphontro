﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Phong_tro
{
    public partial class Bill : Form
    {
        private string workingRoom;
        public string WorkingRoom
        {
            get { return workingRoom; }
            set { workingRoom = value; }
        }

        public Bill()
        {
            InitializeComponent();
            List<DTO.Room> listRoom = DAO.RoomDAO.Instance.LoadRoomList();
            cbRoom.DataSource = listRoom;
            cbRoom.DisplayMember = "TenPhong"; 
        }

        private void tbUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            //MessageBox.Show(cb.SelectedIndex.ToString(), cb.SelectedIndex.GetType().ToString());
            List<DTO.Room> listRoom = DAO.RoomDAO.Instance.LoadRoomList();
            workingRoom = listRoom[cb.SelectedIndex].MaPhong.ToString();
        }


        private void bCreate_Click(object sender, EventArgs e)
        {
            if(tbElec.Text == "" || tbWater.Text == "")
            {
                MessageBox.Show("Nhập thiếu!");
                return;
            }
            string query = "exec dbo.CreateBill @IDPhong = " + workingRoom + 
                ", @Dien = " + tbElec.Text + ", @Nuoc = " + tbWater.Text + ", @Ky = 0";
            try
            {
                DAO.DataProvider.Instance.ExecuteNonQuery(query);
            }
            catch
            {
                MessageBox.Show("Lỗi rồi!");
                return;
            }
            List<DTO.Bill> listBill= DAO.BillDAO.Instance.LoadBillListByRoom(Int32.Parse(workingRoom));
            DTO.Room room = DAO.RoomDAO.Instance.LoadRoomByID(workingRoom);
            if (listBill.Count > 1)
            {
                richTextBox1.Text = "Điện: " + listBill[listBill.Count - 1].SoDien +
                    " - " + listBill[listBill.Count - 2].SoDien + " = " + (listBill[listBill.Count - 1].SoDien
                     - listBill[listBill.Count - 2].SoDien) + " x 3.000" + " = " + ((listBill[listBill.Count - 1].SoDien
                     - listBill[listBill.Count - 2].SoDien) * 3000);
                richTextBox1.Text += "\n" + "Nước: " + listBill[listBill.Count - 1].SoNuoc+
                    " - " + listBill[listBill.Count - 2].SoNuoc + " = " + (listBill[listBill.Count - 1].SoNuoc
                     - listBill[listBill.Count - 2].SoNuoc) + " x 10.000" + " = " + ((listBill[listBill.Count - 1].SoNuoc
                     - listBill[listBill.Count - 2].SoNuoc) * 10000);
                richTextBox1.Text += "\n" + "Nhà: " + room.Gia;
                richTextBox1.Text += "\n" + "Tổng: " + (room.Gia + (listBill[listBill.Count - 1].SoDien
                     - listBill[listBill.Count - 2].SoDien) * 3000 + (listBill[listBill.Count - 1].SoNuoc
                     - listBill[listBill.Count - 2].SoNuoc) * 10000);
            }
        }

        private void bExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bListBill_Click(object sender, EventArgs e)
        {
            try
            {
                List<DTO.Bill> listBill = DAO.BillDAO.Instance.LoadBillListByRoom(Int32.Parse(workingRoom));
                foreach (DTO.Bill a in listBill)
                {
                    richTextBox1.Text += a.MaPhong + ": Ky: " + a.Ky + "  điện-" + a.SoDien
                        + "  nước-" + a.SoNuoc + "Ngày tính: " + a.NgayTinh;
                    richTextBox1.Text += "\n";
                }
            }
            catch
            {
                MessageBox.Show("Lỗi");
                return;
            }
            
        }

        private void Bill_Load(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
