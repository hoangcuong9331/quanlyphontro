﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Phong_tro
{
    public partial class addClient : Form
    {
        private string workingRoom;
        public string WorkingRoom
        {
            get { return workingRoom; }
            set { workingRoom = value; }
        }
        private string roomName;
        public string RoomName
        {
            set { roomName = value; }
            get { return roomName; }
        }
        public addClient()
        {
            
            InitializeComponent();
        }


        private void bExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tbWorkingRoom_VisibleChanged(object sender, EventArgs e)
        {
            lbWorkingRoom.Text = roomName;
        }

        private void bLogon_Click(object sender, EventArgs e)
        {
            int cmnd = 0, age = 0;

            if (Int32.TryParse(tbcmnd.Text, out cmnd) && Int32.TryParse(tbage.Text, out age) && tbName.Text != "")
            {
                string query = "exec dbo.addClient @IDPhong = " + workingRoom + ", @CMND = " + cmnd + ", @Ten = N'" + tbName.Text + 
                    "', @Tuoi = " + age + ", @Gioi = N'" + tbsex.Text + "', @QueQuan = N'" + tborigin.Text + "', @SDT = " + tbSDT.Text;
                try {
                    DAO.DataProvider.Instance.ExecuteNonQuery(query);
                    tbName.Text = "";
                    tbcmnd.Text = "";
                    tbage.Text = "";
                    tbsex.Text = "";
                    tborigin.Text = "";
                    tbSDT.ResetText();
                }
                catch
                {
                    MessageBox.Show("Gặp lỗi!");
                    return;
                }
                MessageBox.Show("Đã thêm 1 khách!");
            }
            else
            {
                MessageBox.Show("Bạn chưa điền đầy đủ");
            }
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
