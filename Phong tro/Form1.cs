﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Phong_tro
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void bLogin_Click(object sender, EventArgs e)
        {
            if (DAO.AccountDAO.Instance.Login(tbUserName.Text, tbPass.Text))
            {
                All all = new All();

                this.Hide();

                all.ShowDialog();

                this.Show();
            }
            else
            {
                MessageBox.Show("Mật khẩu hoặc tên đăng nhập sai!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

        }

        private void bExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show("Bạn có muốn thoát?", "Thông báo", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
            {
                e.Cancel = true;
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void bLogon_Click(object sender, EventArgs e)
        {
            Form2 logon = new Form2();

            this.Hide();

            logon.ShowDialog();

            this.Show();
        }
    }
}
