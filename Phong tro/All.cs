﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Phong_tro
{
    public partial class All : Form
    {
        string workingRoom = "1";
        bool changeRoomMode = false;
        public All()
        {
            InitializeComponent();

            RoomList();
        }

        void RoomList()
        {
            flowLayoutPanel1.Controls.Clear();
            List<DTO.Room> listRoom = DAO.RoomDAO.Instance.LoadRoomList();
            if(listRoom.Count == 0)
            {
                Setup setup = new Setup();

                setup.ShowDialog();
            }
            listRoom = DAO.RoomDAO.Instance.LoadRoomList();
            foreach (DTO.Room item in listRoom)
            {
                Button btn = new Button() { Width = 100, Height = 100 };
                btn.Tag = item.MaPhong.ToString();
                if (item.TRONG == 1)
                {
                    btn.Text = item.TenPhong + "\nTrống";
                    btn.BackColor = Color.FromName("SlateBlue");
                }
                else
                {
                    btn.Text = item.TenPhong + "\nCó người";
                    btn.BackColor = Color.FromName("Tomato");
                }
                btn.Click += btn_Click;
                flowLayoutPanel1.Controls.Add(btn);
            }
        }
        void btn_Click(object sender, EventArgs e)
        {
            if (changeRoomMode == false)
            {
                bAddClien.Enabled = true;
                bElement.Enabled = false;
                bClient.Enabled = true;
                bChangeRoom.Enabled = true;
                bEnempty.Enabled = true;
                bBill.Enabled = true;
                List<DTO.Room> listRoom = DAO.RoomDAO.Instance.LoadRoomList();
                foreach (DTO.Room item in listRoom)
                {
                    if (item.MaPhong.ToString() == (sender as Button).Tag as string)
                    {
                        List<DTO.Room> room = new List<DTO.Room>();
                        room.Add(item);
                        dgvRoom.DataSource = room;
                        TableName.Text = item.TenPhong;
                        workingRoom = item.MaPhong.ToString();
                        break;
                    }
                }
            }
            else
            {
                bAddClien.Enabled = true;
                bElement.Enabled = false;
                bClient.Enabled = true;
                bAddroom.Enabled = true;
                bEnempty.Enabled = true;
                string query = ""; //  exec dbo.changeRoom @IDfrom = 1, @IDto = 2
                try
                {
                    DAO.DataProvider.Instance.ExecuteNonQuery(query);
                }
                catch
                {
                    MessageBox.Show("Lỗi rồi");
                    return;
                }
                label1.Text = "Danh sách phòng";
                changeRoomMode = false;
            }
        }

        private void Logout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bElement_Click(object sender, EventArgs e)
        {
            bElement.Enabled = false;
            bClient.Enabled = true;
            List<DTO.Room> listRoom = DAO.RoomDAO.Instance.LoadRoomList();
            foreach (DTO.Room item in listRoom)
            {
                if (item.MaPhong.ToString() == workingRoom)
                {
                    List<DTO.Room> room = new List<DTO.Room>();
                    room.Add(item);
                    dgvRoom.DataSource = room;
                    break;
                }
            }
        }

        private void bClient_Click(object sender, EventArgs e)
        {
            bClient.Enabled = false;
            bElement.Enabled = true;
            List<DTO.Client> listClient = DAO.ClientDAO.Instance.LoadClientListByRoomID(workingRoom);
            dgvRoom.DataSource = listClient;
        }

        private void Enempty_Click(object sender, EventArgs e)
        {
            string query = "exec dbo.enEmptyRoom @IDPhong = " + workingRoom;
            try
            {
                DAO.DataProvider.Instance.ExecuteNonQuery(query);
            }
            catch
            {
                MessageBox.Show("Lỗi rồi");
                return;
            }
            MessageBox.Show("Phòng đã trống");
            RoomList();
        }

        private void bAddroom_Click(object sender, EventArgs e)
        {
            AddRoom add = new AddRoom();
            this.Hide();
            add.ShowDialog();
            RoomList();
            this.Show();
        }

        private void bChangeRoom_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Đang thi công :)");
            return;
            if (changeRoomMode == true)
            {
                if(MessageBox.Show("Thoát khỏi chế độ chuyển phòng?", "Thông báo", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                    changeRoomMode = false;
                    bAddClien.Enabled = true;
                    bElement.Enabled = false;
                    bClient.Enabled = true;
                    bAddroom.Enabled = true;
                    bEnempty.Enabled = true;
                    label1.Text = "Danh sách phòng";
                }
            }
            else
            {
                changeRoomMode = true;
                bAddClien.Enabled = false;
                bElement.Enabled = false;
                bClient.Enabled = false;
                bAddroom.Enabled = false;
                bEnempty.Enabled = false;
                label1.Text = "Chọn phòng cần chuyển đến";
            }
        }

        private void bAddClien_Click(object sender, EventArgs e)
        {
            using (addClient addc = new addClient())
            {
                this.Hide();
                addc.WorkingRoom = workingRoom;
                addc.RoomName = TableName.Text;
                addc.ShowDialog();
                RoomList();
                this.Show();
            }
        }

        private void bBill_Click(object sender, EventArgs e)
        {
            using (Bill bill = new Bill())
            {
                this.Hide();
                bill.WorkingRoom = workingRoom;
                bill.ShowDialog();
                RoomList();
                this.Show();
            }
        }

        private void bReset_Click(object sender, EventArgs e)
        {
            string query = "exec dbo.ResetDBIncludingAll";
            try
            {
                DAO.DataProvider.Instance.ExecuteNonQuery(query);
                MessageBox.Show("Đã xóa!");
                Close();
            }
            catch
            {
                MessageBox.Show("Lỗi rồi!");
            }
        }

        private void bAddService_Click(object sender, EventArgs e)
        {
            AddService ser = new AddService();
            this.Hide();
            ser.WorkingRoom = workingRoom;
            ser.ShowDialog();
            this.Show();
        }

        private void dgvRoom_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bAnalyzed_Click(object sender, EventArgs e)
        {
            
        }
    }
}
